<?php
/**
 * Country
 *
 * @author Greg Bueno
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model {

	protected $table = 'mw_countries';
	protected $primaryKey = 'country_id';
	protected $softDelete = true;
	protected $fillable = array(
		'country_name',
	);
	protected $guarded = array(
		'country_id',
	);

	public function geocodes() {
		return $this->hasMany('App\Models\Release', 'release_country_name', 'country_name');
	}
}
