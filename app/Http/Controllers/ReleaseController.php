<?php
namespace App\Http\Controllers;

use App\Models\Track;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;
use App\Models\Album;
use App\Models\Country;
use App\Models\Release;
use App\Models\ReleaseFormat;

class ReleaseController extends Controller {

	private $layout_variables = array();

	public function __construct() {
		global $config_url_base;

		$this->layout_variables = array(
			'config_url_base' => $config_url_base,
		);

	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$album_id = Request::get('album');
		if (!empty($album_id)) {
			$releases = Release::where('release_album_id', $album_id)->orderBy('release_catalog_num')->get();
		} else {
			$releases = Release::orderBy('release_catalog_num')->get();
		}
		$releases->load('album');

		$method_variables = array(
			'releases' => $releases,
		);

		$data = array_merge($method_variables, $this->layout_variables);

		return View::make('release.index', $data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

		$release = new Release;
		$release->release_release_date = date('Y-m-d');

		$album_id = Request::get('album');

		if (!empty($album_id)) {
			$release->release_album_id = $album_id;
			$release->album = Album::find($album_id);
			$albums = Album::where('album_artist_id', $release->album->album_artist_id)->orderBy('album_title')->pluck('album_title', 'album_id');
		} else {
			$albums = Album::orderBy('album_title')->pluck('album_title', 'album_id');
		}
        $albums->prepend('&nbsp;', 0);

		$formats = ReleaseFormat::all()->pluck('format_alias', 'format_id')->prepend('&nbsp;', 0);

		$countries = Country::orderBy('country_name')->pluck('country_name', 'country_name')->prepend('&nbsp;', 0);


		$method_variables = array(
			'release' => $release,
			'albums' => $albums,
			'formats' => $formats,
			'countries' => $countries,
		);

		$data = array_merge($method_variables, $this->layout_variables);

		return View::make('release.create', $data);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$release = new Release;

		$fields = $release->getFillable();

		foreach ($fields as $field) {
			$release->{$field} = Request::get($field);
		}

		$result = $release->save();

		if ($result !== false) {
			return Redirect::route('release.show', array('release' => $release->release_id))->with('message', 'Your changes were saved.');
		} else {
			return Redirect::route('album.show', array('album' => $release->release_album_id) )->with('error', 'Your changes were not saved.');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$track_model = new Track();
		$id->release_track_list = $track_model->findReleaseTracks($id->release_id);
		//$id->ecommerce->sortBy('ecommerce_list_order');

		$method_variables = array(
			'release' => $id,
		);

		$data = array_merge($method_variables, $this->layout_variables);

		return View::make('release.show', $data);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$albums = Album::where('album_artist_id', $id->album->album_artist_id)->orderBy('album_title')->pluck('album_title', 'album_id')->prepend('&nbsp;', 0);

		$formats = ReleaseFormat::pluck('format_alias', 'format_id')->prepend('&nbsp;', 0);

		$countries = Country::orderBy('country_name')->pluck('country_name', 'country_name')->prepend('&nbsp;', 0);

		$method_variables = array(
			'release' => $id,
			'albums' => $albums,
			'formats' => $formats,
			'countries' => $countries,
		);

		$data = array_merge($method_variables, $this->layout_variables);

		return View::make('release.edit', $data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$fields = $id->getFillable();

		foreach ($fields as $field) {
			$id->{$field} = Request::get($field);
		}

		$result = $id->save();

		if ($result !== false) {
			return Redirect::route('release.show', array('release' => $id->release_id))->with('message', 'Your changes were saved.');
		} else {
			return Redirect::route('album.show', array('album' => $id->release_album_id))->with('error', 'Your changes were not saved.');
		}
	}


	/**
	 * Show the form for deleting the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function delete($id) {

		$method_variables = array(
			'release' => $id,
		);

		$data = array_merge($method_variables, $this->layout_variables);

		return View::make('release.delete', $data);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$confirm = (boolean) Request::get('confirm');
		$release_catalog_num = $id->release_catalog_num;
		$album_id = $id->release_album_id;

		if ($confirm === true) {
			/*
			 * Tracks are currently not supported, but this bit of logic will be available if/when they are.
			$ecommerce_tracks = Track::with('ecommerce')->where('track_release_id', $id->release_id)->get();
			foreach ($ecommerce_tracks as $ecommerce_track) {
				$ecommerce_track->ecommerce()->delete();
			}
			 */

			// Remove ecommerce.
			$id->ecommerce()->delete();

			// Remove tracks.
			$id->tracks()->delete();

			// Remove releases.
			$id->delete();
			return Redirect::route('album.show', array('album' => $album_id  ))->with('message', 'The record was deleted.');
		} else {
			return Redirect::route('release.show', array('release' => $id->release_id))->with('error', 'The record was not deleted.');
		}
	}

}
