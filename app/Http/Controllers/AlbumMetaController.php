<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;
use \Guzzle\Http\Client;
use \MusicBrainz\Filters\ArtistFilter;
use \MusicBrainz\HttpAdapters\GuzzleHttpAdapter;
use \MusicBrainz\MusicBrainz;
use App\Models\Artist;
use App\Models\Album;
use App\Models\AlbumMeta;

class AlbumMetaController extends Controller {

	private $layout_variables = array();

	public function __construct() {
		global $config_url_base;

		$this->layout_variables = array(
			'config_url_base' => $config_url_base,
		);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  int  $id ID of album to which the meta data is related
	 * @return Response
	 */
	public function store($id)
	{
		$fields = Request::all();

		$result = true;

		foreach ($fields as $field => $value) {
			$meta_item = new AlbumMeta;
			if (($field != '_method' && $field != '_token') && !empty($value)) {
				$meta_item->meta_album_id = $id;
				$meta_item->meta_field_name = $field;
				$meta_item->meta_field_value = $value;

				$save_result = $meta_item->save();

				if ($save_result === false) {
					$result = false;
				}
			}
		}

		if ($result !== false) {
			return Redirect::route('album.show', array('album' => $id))->with('message', 'Your changes were saved.');
		} else {
			return Redirect::route('album.show', array('album' => $id))->with('error', 'Your changes were not saved.');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$album = Album::find($id);
		$artists = Artist::whereHas('meta', function ($query) { $query->where('meta_field_name', 'is_classical_artist')->where('meta_field_value', 1); })->orderBy('artist_last_name')->get()->pluck('artist_display_name', 'artist_id')->prepend('&nbsp;', 0);

		$method_variables = array(
			'album' => $album,
			'artists' => $artists,
		);

		$data = array_merge($method_variables, $this->layout_variables);

		return View::make('album.meta.edit', $data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$meta = AlbumMeta::where('meta_album_id', $id)->get();

		if ($meta->count() == 0) {
			return $this->store($id);
		}

		$fields = Request::all();

		foreach ($fields as $field => $value) {
			if ($field != '_method' && $field != '_token') {
				$meta->{$field} = $value;
			}
		}

		$result = $meta->save();

		if ($result !== false) {
			return Redirect::route('album.show', array('album' => $id))->with('message', 'Your changes were saved.');
		} else {
			return Redirect::route('album.show', array('album' => $id))->with('error', 'Your changes were not saved.');
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
