<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;
use App\Models\Album;
use App\Models\Release;
use App\Models\ReleaseMeta;

class ReleaseMetaController extends Controller {

	private $layout_variables = array();

	public function __construct() {
		global $config_url_base;

		$this->layout_variables = array(
			'config_url_base' => $config_url_base,
		);

	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($id)
	{
		$fields = Request::all();

		foreach ($fields as $field => $value) {
			$meta_item = new ReleaseMeta;
			if (($field != '_method' && $field != '_token') && !empty($value)) {
				$meta_item->meta_release_id = $id;
				$meta_item->meta_field_name = $field;
				$meta_item->meta_field_value = $value;

				$result = true;

				$save_result = $meta_item->save();

				if ($save_result === false) {
					$result = false;
				}
			}
		}

		if ($result !== false) {
			return Redirect::route('album.show', array('album' => $id))->with('message', 'Your changes were saved.');
		} else {
			return Redirect::route('album.show', array('album' => $id))->with('error', 'Your changes were not saved.');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$release = Release::find($id);

		$method_variables = array(
			'release' => $release,
		);

		$data = array_merge($method_variables, $this->layout_variables);

		return View::make('release.meta.edit', $data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$meta = ReleaseMeta::where('meta_release_id', $id)->get();

		if ($meta->count() == 0) {
			return $this->store($id);
		}

		$fields = Request::all();

		foreach ($fields as $field => $value) {
			if ($field != '_method' && $field != '_token') {
				$meta->{$field} = $value;
			}
		}

		$result = $meta->save();

		if ($result !== false) {
			return Redirect::route('release.show', array('release' => $id))->with('message', 'Your changes were saved.');
		} else {
			return Redirect::route('release.show', array('release' => $id))->with('error', 'Your changes were not saved.');
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
}
