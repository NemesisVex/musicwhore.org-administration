<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', array( 'as' => 'admin.home', 'uses' => 'HomeController@index' ));

    // Artist
    Route::model('artist', 'App\Models\Artist');
    Route::get( '/artist/{artist}/delete', array( 'as' => 'artist.delete', 'uses' => 'ArtistController@delete' ) );
    Route::resource('artist', 'ArtistController');

    // ArtistMeta
    Route::model('artist-meta', 'App\Models\ArtistMeta');
    Route::resource('artist-setting', 'ArtistMetaController');

    // Personnel
    Route::model('personnel', 'App\Models\Personnel');
    Route::get( '/personnel/{personnel}/delete', array( 'as' => 'personnel.delete', 'uses' => 'PersonnelController@delete' ) );
    Route::post( '/personnel/save-order', array( 'as' => 'personnel.save-order', 'before' => 'csrf', 'uses' => 'PersonnelController@save_order' ) );
    Route::resource('personnel', 'PersonnelController');

    // Album
    Route::model('album', 'App\Models\Album');
    Route::get( '/album/{album}/delete', array( 'as' => 'album.delete', 'uses' => 'AlbumController@delete' ) );
    Route::resource('album', 'AlbumController');

    // AlbumMeta
    Route::model('album-meta', 'App\Models\AlbumMeta');
    Route::resource('album-setting', 'AlbumMetaController');

    // AlbumMusicbrainz
    Route::post( '/album-musicbrainz/search', array( 'as' => 'album-musicbrainz.search', 'uses' => 'AlbumMusicbrainzController@index' ) );
    Route::resource( 'album-musicbrainz', 'AlbumMusicbrainzController' );

    // AlbumDiscogs
    Route::post( '/album-discogs/search', array( 'as' => 'album-discogs.search', 'uses' => 'AlbumDiscogsController@index' ) );
    Route::resource( 'album-discogs', 'AlbumDiscogsController' );

    // Release
    Route::model('release', 'App\Models\Release');
    Route::get( '/release/{release}/delete', array( 'as' => 'release.delete', 'uses' => 'ReleaseController@delete' ) );
    Route::resource('release', 'ReleaseController');

    // ReleaseMeta
    Route::model('release-meta', 'App\Models\ReleaseMeta');
    Route::resource('release-setting', 'ReleaseMetaController');

    // Tracks
    Route::model('track', 'App\Models\Track');
    Route::get( '/track/{track}/delete', array( 'as' => 'track.delete', 'uses' => 'TrackController@delete' ) );
    Route::post( '/track/save-order', array( 'as' => 'track.save-order', 'before' => 'csrf', 'uses' => 'TrackController@save_order' ) );
    Route::resource('track', 'TrackController');

    // TrackMeta
    Route::model('track-meta', 'App\Models\TrackMeta');
    Route::resource('track-setting', 'TrackMetaController');
});



// Authentication
Auth::routes();

//Route::get( '/login', array( 'as' => 'auth.login', 'uses' => 'AuthController@login') );
//Route::get( '/logout', array( 'as' => 'auth.logout', 'uses' => 'AuthController@sign_out' ) );
//Route::post( '/signin', array( 'as' => 'auth.signin', 'uses' => 'AuthController@sign_in' ) );
